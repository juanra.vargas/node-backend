require('./config/config');

const express = require('express');

const app = express();


const mongoose = require('mongoose');
const bodyParser = require('body-parser')

app.use(bodyParser.urlencoded({ extended: false }));


app.use(bodyParser.json());

app.use(require('./routes/index'))

mongoose.connect(process.env.URLDB, { useNewUrlParser: true, useCreateIndex: true, useFindAndModify: false })
    .then(() => {
        console.log('Base de datos online');
    })
    .catch((err) => {
        console.log(process.env.URLDB);
        console.log(err);
    });

app.listen(process.env.PORT, () => {
    console.log('Escuchando en el puerto ', process.env.PORT);
});