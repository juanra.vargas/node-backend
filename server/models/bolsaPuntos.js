const mongoose = require('mongoose');

const uniqueValidator = require('mongoose-unique-validator');

let Schema = mongoose.Schema;

let bolsaPuntosSchema = new Schema({
    idCliente: {
        type: Schema.Types.ObjectId,
        ref: 'Cliente'
    },
    fechaAsignacion: {
        type: Date
    },
    fechaCaducidad: {
        type: Date
    },
    puntajeAsignado: {
        type: Number
    },
    puntajeUtilizado: {
        type: Number
    },
    saldoPuntos: {
        type: Number
    },
    montoOperacion: {
        type: Number
    }
});

module.exports = mongoose.model('BolsaPuntos', bolsaPuntosSchema);