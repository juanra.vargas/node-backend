const mongoose = require('mongoose');

const uniqueValidator = require('mongoose-unique-validator');
let Schema = mongoose.Schema;

let conceptoPuntosSchema = new Schema({
    descripcion: {
        type: String,
        required: [true, 'El campo descripcion es requerido']
    },
    puntosRequeridos: {
        type: Number,
        required: [true, 'El campo de puntos requeridos es obligatorio']
    }
});

conceptoPuntosSchema.plugin(uniqueValidator, {
    message: '{PATH} debe ser unico'
});

module.exports = mongoose.model('ConceptoPuntos', conceptoPuntosSchema);