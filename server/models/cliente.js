const mongoose = require('mongoose');

const uniqueValidator = require('mongoose-unique-validator');

let Schema = mongoose.Schema;

let clienteSchema = new Schema({
    nombre: {
        type: String,
        required: [true, 'El nombre es necesario']
    },
    apellido: {
        type: String,
        required: [true, 'El apellido es necesario']
    },
    nroDocumento: {
        type: String,
        required: [true, 'El nro de Documento es necesario']

    },
    tipoDocumento: {
        type: String,
        required: [true, 'El tipo de documento es necesario']
    },
    nacionalidad: {
        type: String,
        required: [true, 'La nacionalidad es necesaria']

    },
    email: {
        type: String,
        unique: [true, 'El email debe ser unico'],
        required: [true, 'El email es necesario']

    },
    telefono: {
        type: String,
        required: [true, 'El telefono es necesario']

    },
    fechaNacimiento: {
        type: Date,
        required: [true, 'La fecha de Vencimiento es necesaria']

    }
});

clienteSchema.plugin(uniqueValidator, {
    message: '{PATH} debe de ser unico'
});

module.exports = mongoose.model('Cliente', clienteSchema);