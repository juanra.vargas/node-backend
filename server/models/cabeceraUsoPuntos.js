const mongoose = require('mongoose');

const uniqueValidator = require('mongoose-unique-validator');

let Schema = mongoose.Schema;

let cabeceraUsoPuntosSchema = new Schema({
    idCliente: {
        type: Schema.Types.ObjectId,
        ref: 'Cliente'
    },
    puntajeUtilizado: {
        type: Number
    },
    fecha: {
        type: Date
    },
    conceptoUsoPuntos: {
        type: Schema.Types.ObjectId,
        ref: 'ConceptoPuntos'
    }
});

module.exports = mongoose.model('CabeceraPuntos', cabeceraUsoPuntosSchema);