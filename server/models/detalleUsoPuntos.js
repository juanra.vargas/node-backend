const mongoose = require('mongoose');

const uniqueValidator = require('mongoose-unique-validator');

let Schema = mongoose.Schema;

let detalleUsoPuntosSchema = new Schema({
    idCabecera: {
        type: Schema.Types.ObjectId,
        ref: 'CabeceraPuntos'
    },
    puntajeUtilizado: {
        type: Object
    },
    idBolsaPuntos: {
        type: Schema.Types.ObjectId,
        ref: 'BolsaPuntos'
    }
});

module.exports = mongoose.model('DetalleUsoPuntos', detalleUsoPuntosSchema);