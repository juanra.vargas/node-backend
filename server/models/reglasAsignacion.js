const mongoose = require('mongoose');

const uniqueValidator = require('mongoose-unique-validator');

let Schema = mongoose.Schema;

let reglasAsignacionSchema = new Schema({
    limiteInferior: {
        type: Number,
        required: [true, 'El campo limite inferior es requerido']
    },
    limiteSuperior: {
        type: Number,
        required: [true, 'El campo limite superior es requerido']
    },
    montoEquivalencia: {
        type: Number,
        required: [true, 'El campo monto es requerido']
    }
});

reglasAsignacionSchema.plugin(uniqueValidator, {
    message: '{PATH} debe ser unico'
});

module.exports = mongoose.model('Reglas', reglasAsignacionSchema);