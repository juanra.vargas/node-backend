const mongoose = require('mongoose');

const uniqueValidator = require('mongoose-unique-validator');

let Schema = mongoose.Schema;

let vencimientoPuntosSchema = new Schema({
    fechaInicio: {
        type: Date,
        required: [true, 'El campo de fecha inicio es requerido']
    },
    fechaFin: {
        type: Date,
        required: [true, 'El campo de fecha fin es requerido']
    },
    diasDuracion: {
        type: Number, 
        required: [true, 'El campo dias de duracion es requerido']
    }
});


module.exports = mongoose.model('VencimientoPuntos', vencimientoPuntosSchema);