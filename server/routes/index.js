const express = require('express');

const app = express();

app.use(require('./cliente'));
app.use(require('./usoPuntos'));
app.use(require('./reglasAsignacion'));
app.use(require('./vencimientoPuntos'));
app.use(require('./cabeceraUsoPuntos'));
app.use(require('./bolsaPuntos'));
app.use(require('./usuario'));
module.exports = app;