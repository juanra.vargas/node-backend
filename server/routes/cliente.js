const express = require('express');
const _ = require('underscore');

const Cliente = require('../models/cliente.js')
const app = express();

app.get('/sis-fidelizacion', function (req, res) {
    res.json({
        ok: true,
        message: 'Hola Mundo'
    });
})

app.get('/sis-fidelizacion/cliente', function (req, res) {
    Cliente.find({})
        .skip(0)
        .limit(5)
        .exec((err, cliente) => {
            if (err) {
                return res.status(400).json({
                    ok: false,
                    err
                })
            }
            return res.json({
                ok: true,
                cliente
            });
        });
});
app.get('/sis-fidelizacion/cliente/:id', function (req, res) {
    let id = req.params.id;
    Cliente.findById(id, (err, clienteDB) => {
        if (err) {
            return res.json({
                ok: false,
                err
            })
        }
        res.json({
            ok: true,
            cliente: clienteDB
        });
    });
});

app.get('/sis-fidelizacion/cliente/nroDocumento/:nroDocumento', function (req, res) {
    console.log(req.params);
    Cliente.find({
        nroDocumento: req.params.nroDocumento
    }).exec((err, cliente) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                err
            })
        }
        res.json({
            ok: true,
            cliente
        });
    })
});

app.post('/sis-fidelizacion/cliente', function (req, res) {

    let body = req.body;

    console.log(body);


    let cliente = new Cliente({
        nombre: body.nombre,
        apellido: body.apellido,
        nroDocumento: body.nroDocumento,
        tipoDocumento: body.tipoDocumento,
        nacionalidad: body.nacionalidad,
        email: body.email,
        telefono: body.telefono,
        fechaNacimiento: body.fechaNacimiento
    });
    console.log(cliente);
    cliente.save((err, clienteDB) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                err
            });
        }
        //clienteDB.password = null;
        res.json({
            ok: true,
            cliente: clienteDB
        });
    });

});

app.put('/sis-fidelizacion/cliente/:id', function (req, res) {

    let id = req.params.id;

    let body = req.body;

    Cliente.findByIdAndUpdate(id, body, { new: true, runValidators: true }, (err, clienteDB) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                err
            });
        }

        res.json({
            ok: true,
            clienteDB
        });
    });

});

app.delete('/sis-fidelizacion/cliente/:id', function (req, res) {

    let id = req.params.id;
    Cliente.findByIdAndDelete(id, (err, clienteDB) => {
        if (err) {
            return res.status(500).json({
                ok: false,
                err
            });
        }
        res.json({
            ok: true,
            clienteDB
        })
    });
});

module.exports = app;