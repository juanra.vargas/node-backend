const express = require('express');

const app = express();

const VencimientoPuntos = require('../models/vencimientoPuntos');


app.get('/sis-fidelizacion/vencimiento', function (req, res) {
    VencimientoPuntos.find({})
        .exec((err, listVencimiento) => {
            if (err) {
                return res.status(400).json({
                    ok: false,
                    err
                })
            }
            res.json({
                ok: true,
                vencimientos: listVencimiento
            });
        });
});

app.post('/sis-fidelizacion/vencimiento/filtro', function (req, res) {
    /***
     * Se asume que en el body se envian 2 parametros obligatorios
     * Si el campo HASTA no se incluye se toma la fecha de del dia
     * Si el campo DESDE no se incluye se toman los registros desde el 01/01/1982
     * 
     * El formato de la fecha a enviar es MM/DD/YYYY
     * 
     */
    if (req.query.desde === undefined || req.query.desde === 'null') {
        var desde = new Date('01-01-1982');
    } else {
        var desde = (new Date(req.query.desde));
    }

    if (req.query.hasta === undefined || req.query.hasta === 'null') {
        var hasta = new Date()
    } else {
        var hasta = new Date(req.query.hasta);
    }

    console.log(desde, '----', hasta);
    VencimientoPuntos.find({
        fechaInicio: { $gte: desde },
        fechaFin: { $lte: hasta }
    }).exec((err, array) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                err
            })
        }
        res.json({
            array
        });
    });
});

app.post('/sis-fidelizacion/vencimiento', function (req, res) {
    let body = req.body;
    console.log(body);
    let fechaInicio = new Date(body.fechaInicio);
    let fechaFin = new Date(body.fechaFin);
    let diasDuracion = req.body.diasDuracion
    console.log(fechaInicio, fechaFin, diasDuracion);

    let vencimientoPuntos = new VencimientoPuntos({
        fechaInicio,
        fechaFin,
        diasDuracion
    });
    console.log(vencimientoPuntos);

    vencimientoPuntos.save((err, vencimientoPuntosDB) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                err
            });
        }
        res.json({
            ok: true,
            vencimientoPuntosDB
        });
    });
});

Date.daysBetween = function (date1, date2) {
    //Get 1 day in milliseconds
    var one_day = 1000 * 60 * 60 * 24;

    // Convert both dates to milliseconds
    var date1_ms = date1.getTime();
    var date2_ms = date2.getTime();

    // Calculate the difference in milliseconds
    var difference_ms = date2_ms - date1_ms;

    // Convert back to days and return
    return Math.round(difference_ms / one_day);
}

app.put('/sis-fidelizacion/vencimiento/:id', function (req, res) {
    let id = req.params.id;
    let body = req.body;
    console.log(id);
    VencimientoPuntos.findByIdAndUpdate(id, body, { new: true, runValidators: true }, (err, vencimientoDB) => {
        if (err) {
            return res.status(400)
                .json({
                    ok: false,
                    err
                });
        }
        console.log(vencimientoDB.fechaInicio);
        console.log(vencimientoDB.fechaFin);

        res.json({
            vencimientoDB
        });

    });
});

app.delete('/sis-fidelizacion/vencimiento/:id', function (req, res) {
    let id = req.params.id;
    VencimientoPuntos.findByIdAndDelete(id, (err, vencimientoDB) => {
        if (err) {
            return res.status(500).json({
                ok: false,
                err
            });
        }
        res.json({
            ok: true,
            vencimientoDB
        });
    });

});
module.exports = app;