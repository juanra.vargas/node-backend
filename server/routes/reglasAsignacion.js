const express = require('express');

const app = express();

const ReglasAsignacion = require('../models/reglasAsignacion');

app.get('/sis-fidelizacion/reglas', function (req, res) {
    ReglasAsignacion.find({})
        .exec((err, reglasAsignacionDB) => {
            if (err) {
                return res.status(400).json({
                    ok: false,
                    err
                });
            }
            res.json({
                ok: true,
                reglas: reglasAsignacionDB
            })
        });
});


app.get('/sis-fidelizacion/reglas/:id', function (req, res) {
    console.log(req.params);
    let id = req.params.id;

    ReglasAsignacion.findById(id, (err, reglaAsignacionDB) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                err
            });
        }
        res.json({
            ok: true, 
            regla: reglaAsignacionDB
        });
    })
});

app.post('/sis-fidelizacion/reglas', function (req, res) {
    let body = req.body;
    console.log(body);


    let reglaAsinacion = new ReglasAsignacion({
        limiteInferior: body.limiteInferior,
        limiteSuperior: body.limiteSuperior,
        montoEquivalencia: body.montoEquivalencia
    });
    console.log(reglaAsinacion);

    reglaAsinacion.save((err, reglaAsignacionDB) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                err
            });
        }


        console.log('Regla de asignacion creada');
        res.json({
            ok: true,
            reglaAsignacion: reglaAsignacionDB
        });
    });
});

app.put('/sis-fidelizacion/reglas/:id', function (req, res) {
    let id = req.params.id;
    let body = req.body;

    ReglasAsignacion.findByIdAndUpdate(id, body, { new: true, runValidators: true }, (err, reglasDB) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                err
            })
        }
        res.json({
            ok: true,
            reglasDB
        });
    });
});

app.delete('/sis-fidelizacion/reglas/:id', function (req, res) {
    let id = req.params.id;
    ReglasAsignacion.findByIdAndDelete(id, (err, reglaDB) => {
        if (err) {
            return res.status(500).json({
                ok: false,
                err
            });
        }
        res.json({
            ok: true,
            reglaDB
        });
    });
});

app.post('/sis-fidelizacion/reglas/consultar-puntos', function (req, res) {
    let body = req.body;
    console.log(body);
    monto = body.monto;
    ReglasAsignacion.find({ limiteInferior: { $lte: monto }, limiteSuperior: { $gte: monto } })
        .exec((err, reglas) => {
            console.log('realizando consulta');
            if (err) {
                return res.json({
                    ok: false,
                    err
                });
            }
            if(reglas.length === 0){
                return res.json({
                    ok: false, 
                    message: 'No se tienen puntos para ese monto'
                });
            }
            let regla = reglas[0];
            let montoEquivalencia = regla.montoEquivalencia;
            let puntos;
            try {
                console.log(Math.trunc(monto / montoEquivalencia));
                puntos = Math.trunc(monto / montoEquivalencia);
            } catch (error) {
                throw error;
            }


            res.json({
                ok: true,
                puntos
            });
        });
});

module.exports = app;