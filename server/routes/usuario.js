const express = require('express');
const Usuario = require('../models/usuario');
const app = express();

app.get('/sis-fidelizacion/usuario', function (req, res) {
    Usuario.find({}).exec((err, usuarios) => {
        if (err) {
            return res.json({
                ok: false,
                err
            });
        }
        res.json({
            ok: true,
            usuarios
        });
    });
});

app.post('/sis-fidelizacion/usuario/autenticar', function (req, res) {
    Usuario.find({ email: req.body.email }).exec((err, usuarios) => {
        if (err) {
            return res.json({
                ok: false,
                err
            });
        }
        let usuario = usuarios[0]
        console.log(usuario);
        if (usuario.password === req.body.password) {
            return res.json({
                ok: true,
                message: 'Usuario existente en el sistema'
            });
        }
        res.status(400).json({
            ok: false,
            message: 'Usuario o passwords incorrectos'
        });
    });
});


app.post('/sis-fidelizacion/usuario', function (req, res) {
    let body = req.body;
    console.log(body);
    let usuario = new Usuario({
        nombre: body.nombre,
        email: body.email,
        password: body.password
    });

    usuario.save((err, usuarioDB) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                err
            });
        }
        console.log(usuarioDB);
        res.json({
            ok: true,
            usuario: usuarioDB
        });
    });
});

app.put('/sis-fidelizacion/usuario/:id', function (req, res) {
    let id = req.params.id;

    let body = req.body;

    Usuario.findByIdAndUpdate(id, body, { new: true, runValidators: true }, (err, usuarioDB) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                err
            });
        }

        res.json({
            ok: true,
            usuario: usuarioDB
        });
    });
});

app.delete('/sis-fidelizacion/usuario/:id', function (req, res) {

    let id = req.params.id;
    console.log(id);
    Usuario.findByIdAndDelete(id, (err, usuarioDB) => {
        if (err) {
            return res.status(500).json({
                ok: false,
                err
            });
        }
        console.log(usuarioDB);
        res.json({
            ok: true,
            usuarioDB
        });
    });
});

module.exports = app;