const express = require('express');
const _ = require('underscore');
const CabeceraUsoPuntos = require('../models/cabeceraUsoPuntos');
const BolsaPuntos = require('../models/bolsaPuntos');
const DetalleUsoPuntos = require('../models/detalleUsoPuntos');
const UsoPuntos = require('../models/conceptoPuntos');
const Cliente = require('../models/cliente');
const app = express();
const mongoose = require('mongoose');
const nodemailer = require('nodemailer');

app.get('/sis-fidelizacion/detalle-uso-puntos/uso-de-puntos', function (req, res) {
    console.log(req.query);
    let fechaDesde = new Date(req.query.fechaDesde);
    let fechaHasta = new Date(req.query.fechaHasta);
    console.log(fechaDesde + '   ----------  ' + fechaHasta);
    let idConcepto = req.query.idConcepto;
    console.log(idConcepto);
    let idCliente = req.query.idCliente;
    console.log(idCliente);
    console.log(req.query);
    console.log(mongoose.Types.ObjectId.isValid(req.query.idCliente));

    if (req.query.idCliente !== 'null' && !mongoose.Types.ObjectId.isValid(req.query.idCliente)) {
        return res.json({
            ok: false,
            message: 'id cliente invalido'
        });
    }
    if (req.query.idConcepto !== 'null' && !mongoose.Types.ObjectId.isValid(req.query.idConcepto)) {
        return res.json({
            ok: false,
            message: 'id concepto invalido'
        });
    }
    if (idConcepto === 'null' && idCliente === 'null' && req.query.fechaDesde === 'null' && req.query.fechaHasta === 'null') {
        console.log(req.query);
        CabeceraUsoPuntos.find({}, (err, cabeceras) => {
            if (err) {
                return res.json({
                    ok: false,
                    err
                });
            }
            res.json({
                ok: true,
                cabeceras
            });
        });
    } else if (idConcepto !== 'null' && idCliente === 'null' && req.query.fechaDesde === 'null' && req.query.fechaHasta === 'null') {
        CabeceraUsoPuntos.find({ conceptoUsoPuntos: idConcepto }, (err, cabeceras) => {
            if (err) {
                return res.json({
                    ok: false,
                    err
                });
            }
            res.json({
                ok: true,
                cabeceras
            });
        });
    } else if (idConcepto === 'null' && idCliente !== 'null' && req.query.fechaDesde === 'null' && req.query.fechaHasta === 'null') {
        CabeceraUsoPuntos.find({ idCliente: idCliente }, (err, cabeceras) => {
            if (err) {
                return res.json({
                    ok: false,
                    err
                });
            }
            res.json({
                ok: true,
                cabeceras
            });
        });
    } else if (idConcepto === 'null' && idCliente === 'null' && req.query.fechaDesde !== 'null' && req.query.fechaHasta !== 'null') {
        CabeceraUsoPuntos.find({ fecha: { $gte: fechaDesde, $lte: fechaHasta } }, (err, cabeceras) => {
            if (err) {
                return res.json({
                    ok: false,
                    err
                });
            }
            res.json({
                ok: true,
                cabeceras
            });
        });
    } else if (idConcepto !== 'null' && idCliente === 'null' && req.query.fechaDesde !== 'null' && req.query.fechaHasta !== 'null') {
        CabeceraUsoPuntos.find({ fecha: { $gte: fechaDesde, $lte: fechaHasta }, conceptoUsoPuntos: idConcepto }, (err, cabeceras) => {
            if (err) {
                return res.json({
                    ok: false,
                    err
                });
            }
            res.json({
                ok: true,
                cabeceras
            });
        });
    } else if (idConcepto === 'null' && idCliente !== 'null' && req.query.fechaDesde !== 'null' && req.query.fechaHasta !== 'null') {
        CabeceraUsoPuntos.find({ fecha: { $gte: fechaDesde, $lte: fechaHasta }, idCliente: idCliente }, (err, cabeceras) => {
            if (err) {
                return res.json({
                    ok: false,
                    err
                });
            }
            res.json({
                ok: true,
                cabeceras
            });
        });
    } else if (idConcepto !== 'null' && idCliente !== 'null' && req.query.fechaDesde === 'null' && req.query.fechaHasta === 'null') {
        CabeceraUsoPuntos.find({ idCliente: idCliente, conceptoUsoPuntos: idConcepto }, (err, cabeceras) => {
            if (err) {
                return res.json({
                    ok: false,
                    err
                });
            }
            res.json({
                ok: true,
                cabeceras
            });
        });
    } else if (idConcepto !== 'null' && idCliente !== 'null' && req.query.fechaDesde !== 'null' && req.query.fechaHasta !== 'null') {
        console.log('Caso todos los parametros enviados');
        CabeceraUsoPuntos.find({ fecha: { $gte: fechaDesde, $lte: fechaHasta }, conceptoUsoPuntos: idConcepto, idCliente: idCliente }, (err, cabeceras) => {
            if (err) {
                return res.json({
                    ok: false,
                    err
                });
            }
            res.json({
                ok: true,
                cabeceras
            });
        });
    }
});


app.get('/sis-fidelizacion/detalle-uso-puntos', function (req, res) {
    CabeceraUsoPuntos.find({})
        .populate('cliente')
        .exec((err, cabeceras) => {
            if (err) {
                return res.json({
                    ok: false,
                    err
                });
            }
            res.json({
                cabeceras
            });
        });
});

app.get('/sis-fidelizacion/detalle-uso-puntos/cliente/:id', function (req, res) {
    let idCliente = req.params.id;
    console.log(idCliente);
    CabeceraUsoPuntos.find({ idCliente: idCliente })
        .exec((err, cabeceras) => {
            if (err) {
                return res.json({
                    ok: false,
                    err
                });
            }
            console.log(cabeceras);
            res.json({
                ok: true,
                puntos: cabeceras
            });
        });
});

app.get('/sis-fidelizacion/detalle-uso-puntos/concepto/:id', function (req, res) {
    let idConcepto = req.params.id;
    console.log(idConcepto);
    CabeceraUsoPuntos.find({ conceptoUsoPuntos: idConcepto })
        .exec((err, cabeceras) => {
            if (err) {
                return res.json({
                    ok: false,
                    err
                });
            }
            console.log(cabeceras);
            res.json({
                ok: true,
                puntos: cabeceras
            })
        });

});

app.post('/sis-fidelizacion/detalle-uso-puntos/utilizar-puntos', function (req, res) {
    const body = req.body;
    console.log(body);

    let idCliente = body.idCliente;
    let idConcepto = body.idConcepto;


    UsoPuntos.findById(idConcepto, (err, concepto) => {
        if (err) {
            return res.json({
                ok: false,
                err
            });
        }
        let totalPuntosRequeridos = concepto.puntosRequeridos;
        let cabecera = new CabeceraUsoPuntos({
            idCliente: idCliente,
            puntajeUtilizado: Number(concepto.puntosRequeridos),
            fecha: new Date(),
            conceptoUsoPuntos: idConcepto
        });
        console.log('Cabecera' + cabecera);
        console.log(cabecera._id);
        cabecera.save((err, cabeceraDB) => {
            if (err) {
                return res.status(400).json({
                    ok: false,
                    err
                });
            }
            console.log('pasa algo');
            // Se deben traer todas las bolsas de puntos con saldo mayor a cero, y que no esten vencidas.
            // Deben estar ordenadas por fecha de caducidad en forma descendente. 
            let fechaActual = new Date();
            BolsaPuntos.find({ saldoPuntos: { $gt: 0 }, fechaCaducidad: { $gte: fechaActual }, idCliente: idCliente })
                .sort({ fechaCaducidad: 'asc' })
                .exec((err, bolsas) => {
                    if (err) {
                        return res.json({
                            ok: false,
                            err
                        });
                    }
                    console.log(bolsas);
                    if (bolsas.length === 0) {
                        return res.json({
                            ok: false,
                            message: 'El cliente no posee bolsas utilizables'
                        });
                    }
                    let suma = 0;
                    for (let index = 0; index < bolsas.length; index++) {
                        suma = suma + bolsas[index].saldoPuntos;
                    }
                    console.log(suma);
                    if (suma < totalPuntosRequeridos) {
                        return res.json({
                            ok: false,
                            message: 'No se poseen puntos suficientes'
                        });
                    }
                    let puntosUtilizados = 0;
                    let i = -1;
                    console.log(bolsas);
                    while (i < bolsas.length - 1 && totalPuntosRequeridos > 0) {
                        i++;
                        console.log(i);
                        let bolsaPuntosAux = bolsas[i];
                        console.log('BolsaPuntosAux: ' + bolsaPuntosAux);
                        if (bolsaPuntosAux === undefined) {
                            return res.json({
                                ok: false,
                                message: 'Bolsa inexistente'
                            });
                        }
                        puntosUtilizados = bolsaPuntosAux.puntajeAsignado - bolsaPuntosAux.saldoPuntos;
                        puntosUtilizados = Number(puntosUtilizados);
                        totalPuntosRequeridos = totalPuntosRequeridos - bolsaPuntosAux.saldoPuntos;
                        totalPuntosRequeridos = Number(totalPuntosRequeridos);
                        if (totalPuntosRequeridos >= 0) {
                            console.log('bolsaPuntosAux' + bolsaPuntosAux);
                            let objectToUpdate = {
                                puntajeUtilizado: Number(bolsaPuntosAux.puntajeAsignado),
                                saldoPuntos: 0
                            }
                            console.log({
                                puntajeUtilizado: Number(bolsaPuntosAux.puntajeAsignado),
                                saldoPuntos: 0
                            });
                            BolsaPuntos.findByIdAndUpdate(bolsaPuntosAux._id, {
                                puntajeUtilizado: Number(bolsaPuntosAux.puntajeAsignado),
                                saldoPuntos: 0
                            }, (err, bolsaDB) => {
                                if (err) {
                                    return res.json({
                                        ok: false,
                                        err
                                    });
                                }
                                console.log(bolsaDB);
                            });
                            let detalle = new DetalleUsoPuntos({
                                idCabecera: cabeceraDB._id,
                                puntajeUtilizado: Number(puntosUtilizados),
                                idBolsaPuntos: bolsaPuntosAux._id
                            });
                            console.log(detalle);
                            console.log('Grabando el detalle');
                            detalle.save((err, detalleDB) => {
                                if (err) {
                                    return res.json({
                                        ok: false,
                                        err
                                    });
                                }
                                //console.log(detalleDB);
                            });
                        } else {
                            totalPuntosRequeridos = Math.abs(totalPuntosRequeridos);
                            let saldoDePuntos = totalPuntosRequeridos;
                            saldoDePuntos = Number(saldoDePuntos);
                            let puntajeUtilizado = bolsaPuntosAux.puntajeAsignado - saldoDePuntos;
                            puntajeUtilizado = Number(puntajeUtilizado);
                            let objectToUpdate = {
                                saldoPuntos: saldoDePuntos,
                                puntajeUtilizado: Number(puntajeUtilizado)
                            }
                            console.log('Objeto a actualizar: ' + objectToUpdate);
                            BolsaPuntos.findByIdAndUpdate(bolsaPuntosAux._id, {
                                saldoPuntos: saldoDePuntos,
                                puntajeUtilizado: Number(puntajeUtilizado)
                            }, (err, bolsaDB) => {
                                if (err) {
                                    return res.json({
                                        ok: false,
                                        err
                                    });
                                }
                                //console.log(bolsaDB);
                            });
                            console.log('Grabando el detalle');
                            console.log('Puntaje Utilizado: ' + puntajeUtilizado);
                            let detalle = new DetalleUsoPuntos({
                                idCabecera: cabeceraDB._id,
                                puntajeUtilizado: Number(puntajeUtilizado),
                                idBolsaPuntos: bolsaPuntosAux._id
                            });
                            console.log('Detalle: ' + detalle);
                            console.log('Pasa algo');
                            detalle.save((err, detalleDB) => {
                                if (err) {
                                    console.log(err);
                                    return res.json({
                                        ok: false,
                                        err
                                    });
                                }
                                //console.log(detalleDB);
                            });

                        }
                    }

                    //Aqui se envia al cliente el correo electronico
                    Cliente.findById(idCliente, (err, clienteDB) => {
                        if (err) {
                            console.log(err);
                            return res.json({
                                ok: false,
                                err
                            });
                        }
                        sendMail(clienteDB.email).catch(console.error);
                        res.json({
                            ok: true,
                            message: 'Puntos utilizados exitosamente'
                        });
                    });
                });
        });
    });
});




// async..await is not allowed in global scope, must use a wrapper
async function sendMail(mailCliente) {

    // Generate test SMTP service account from ethereal.email
    // Only needed if you don't have a real mail account for testing
    let testAccount = await nodemailer.createTestAccount();

    // create reusable transporter object using the default SMTP transport
    let transporter = nodemailer.createTransport({
        host: "smtp.ethereal.email",
        port: 587,
        secure: false, // true for 465, false for other ports
        auth: {
            user: 'rhea42@ethereal.email', // generated ethereal user
            pass: 'Fb4ZXRv7qeq1KW32TF' // generated ethereal password
        }
    });

    // send mail with defined transport object
    let info = await transporter.sendMail({
        from: '"Fred Foo 👻" <foo@example.com>', // sender address
        to: `<${mailCliente}>`, // list of receivers
        subject: "Confirmacion de uso de ✔", // Subject line
        text: "Sus puntos fueron correctamente utilizados?", // plain text body
        html: "<b>Puntos utilizados correctamente?</b>" // html body
    });

    console.log("Message sent: %s", info.messageId);
    // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>

    // Preview only available when sending through an Ethereal account
    console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
    // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
}

//sendMail().catch(console.error);

module.exports = app;