const express = require('express');
const _ = require('underscore');
const BolsaPuntos = require('../models/bolsaPuntos');
const VencimientoPuntos = require('../models/vencimientoPuntos');
const ReglasAsignacion = require('../models/reglasAsignacion');
const app = express();
const mongoose = require('mongoose');
const addSubstractDate = require('add-subtract-date');




app.get('/sis-fidelizacion/bolsa-de-puntos', function(req, res) {

    /****
     * estados: TODOS, VIGENTES, VENCIDOS
     * vencimiento: Number
     * idCliente: ObjectId
     */
    console.log(req.query);
    // Se verifica que el id cliente sea valido
    if (req.query.idCliente !== 'null' && !mongoose.Types.ObjectId.isValid(req.query.idCliente)) {
        return res.status(400).json({
            ok: false,
            message: 'idCliente no valido'
        });
    }

    if (req.query.idCliente === 'null' && req.query.vencimiento === 'null' && (req.query.estado === 'null' || req.query.estado === 'TODOS')) {
        console.log('caso 1');
        BolsaPuntos.find({})
            .populate('idCliente')
            .exec((err, bolsas) => {
                if (err) {
                    return res.json({
                        ok: false,
                        err
                    });
                }
                return res.json({
                    ok: true,
                    bolsas
                });
            });
    } else if (req.query.idCliente !== 'null' && req.query.vencimiento === 'null' && (req.query.estado === 'null' || req.query.estado === 'TODOS')) {
        BolsaPuntos.find({ idCliente: req.query.idCliente })
            .populate('idCliente')
            .exec((err, bolsas) => {
                if (err) {
                    return res.json({
                        ok: false,
                        err
                    });
                }
                console.log(bolsas.length);
                if (bolsas.length === 0) {
                    return res.json({
                        ok: false,
                        message: 'No existen bolsas para el cliente especificado'
                    });
                }
                return res.status(200).json({
                    ok: true,
                    bolsas
                });
            });
    } else if (req.query.idCliente === 'null' && req.query.vencimiento !== 'null' && (req.query.estado === 'null' || req.query.estado === 'TODOS')) {
        let vencimiento = Number(req.query.vencimiento)
        console.log(vencimiento);
        if (isNaN(vencimiento)) {
            return res.json({
                ok: false,
                message: 'El numero de dias no es valido'
            });
        }
        let fecha = new Date();
        console.log(fecha);
        addSubstractDate.add(fecha, vencimiento, "days")
        console.log(fecha);

        BolsaPuntos.find({ fechaCaducidad: fecha })
            .populate('idCliente')
            .exec((err, bolsas) => {
                if (err) {
                    return res.json({
                        ok: false,
                        err
                    });
                }
                if (bolsas.length === 0) {
                    return res.json({
                        ok: false,
                        message: 'No existen bolsas con esa fecha de vencimiento'
                    });
                }
                return res.json({
                    ok: true,
                    bolsas
                });
            });
    } else if (req.query.idCliente === 'null' && req.query.vencimiento === 'null' && req.query.estado === 'VENCIDOS') {
        let fecha = new Date();
        console.log(fecha);

        BolsaPuntos.find({ fechaCaducidad: { $lte: fecha } })
            .populate('idCliente')
            .exec((err, bolsas) => {
                if (err) {
                    return res.json({
                        ok: false,
                        err
                    });
                }
                if (bolsas.length === 0) {
                    return res.json({
                        ok: false,
                        message: 'No existen bolsas vencidas'
                    });
                }
                return res.json({
                    ok: true,
                    bolsas
                });
            });
    } else if (req.query.idCliente === 'null' && req.query.vencimiento === 'null' && req.query.estado === 'VIGENTES') {
        let fecha = new Date();
        console.log(fecha);

        BolsaPuntos.find({ fechaCaducidad: { $gte: fecha } })
            .populate('idCliente')
            .exec((err, bolsas) => {
                if (err) {
                    return res.json({
                        ok: false,
                        err
                    });
                }
                if (bolsas.length === 0) {
                    return res.json({
                        ok: false,
                        message: 'No existen bolsas vigentes'
                    });
                }
                return res.json({
                    ok: true,
                    bolsas
                });
            });
    } else if (req.query.idCliente !== 'null' && req.query.vencimiento !== 'null' && (req.query.estado === 'null' || req.query.estado === 'TODOS')) {
        console.log(req.query);
        let vencimiento = Number(req.query.vencimiento)
        console.log(vencimiento);
        if (isNaN(vencimiento)) {
            return res.json({
                ok: false,
                message: 'El numero de dias no es valido'
            });
        }
        let fecha = new Date();
        console.log(fecha);
        addSubstractDate.add(fecha, vencimiento, "days")
        console.log(fecha);
        BolsaPuntos.find({ fechaCaducidad: fecha, idCliente: req.query.idCliente })
            .populate('idCliente')
            .exec((err, bolsas) => {
                if (err) {
                    return res.json({
                        ok: false,
                        err
                    });
                }
                if (bolsas.length === 0) {
                    return res.json({
                        ok: false,
                        message: 'No existen bolsas con esa fecha de vencimiento que pertenezcan a ese cliente'
                    });
                }
                return res.json({
                    ok: true,
                    bolsas
                });
            });
    } else if (req.query.idCliente !== 'null' && req.query.vencimiento === 'null' && req.query.estado === 'VENCIDOS') {
        let fecha = new Date();
        console.log(fecha);

        BolsaPuntos.find({ fechaCaducidad: { $gte: fecha }, idCliente: req.query.idCliente })
            .populate('idCliente')
            .exec((err, bolsas) => {
                if (err) {
                    return res.json({
                        ok: false,
                        err
                    });
                }
                if (bolsas.length === 0) {
                    return res.json({
                        ok: false,
                        message: 'No existen bolsas vencidas'
                    });
                }
                return res.json({
                    ok: true,
                    bolsas
                });
            });
    } else if (req.query.idCliente !== 'null' && req.query.vencimiento !== 'null' && req.query.estado === 'VIGENTES') {
        let vencimiento = Number(req.query.vencimiento)
        console.log(vencimiento);
        if (isNaN(vencimiento)) {
            return res.json({
                ok: false,
                message: 'El numero de dias no es valido'
            });
        }
        let fechaActual = new Date();
        let fecha = new Date();
        console.log(fecha);
        addSubstractDate.add(fecha, vencimiento, "days")
        console.log(fecha);

        BolsaPuntos.find({ fechaCaducidad: { $gte: fechaActual }, fechaCaducidad: fecha, idCliente: req.query.idCliente })
            .populate('idCliente')
            .exec((err, bolsas) => {
                if (err) {
                    return res.json({
                        ok: false,
                        err
                    });
                }
                if (bolsas.length === 0) {
                    return res.json({
                        ok: false,
                        message: 'No existen bolsas vigentes'
                    });
                }
                return res.json({
                    ok: true,
                    bolsas
                });
            });
    } else if (req.query.idCliente !== 'null' && req.query.vencimiento === 'null' && req.query.estado === 'VIGENTES') {
        let fecha = new Date();
        console.log(fecha);

        BolsaPuntos.find({ fechaCaducidad: { $lte: fecha }, idCliente: req.query.idCliente })
            .populate('idCliente')
            .exec((err, bolsas) => {
                if (err) {
                    return res.json({
                        ok: false,
                        err
                    });
                }
                if (bolsas.length === 0) {
                    return res.json({
                        ok: false,
                        message: 'No existen bolsas vigentes'
                    });
                }
                res.json({
                    ok: true,
                    bolsas
                });
            });
    } else if (req.query.idCliente === 'null' && req.query.vencimiento !== 'null' && req.query.estado === 'VIGENTES') {
        let vencimiento = Number(req.query.vencimiento)
        console.log(vencimiento);
        if (isNaN(vencimiento)) {
            return res.json({
                ok: false,
                message: 'El numero de dias no es valido'
            });
        }
        let fechaActual = new Date();
        let fecha = new Date();
        console.log(fecha);
        addSubstractDate.add(fecha, vencimiento, "days")
        console.log(fecha);
        BolsaPuntos.find({ fechaCaducidad: { $gte: fechaActual }, fechaCaducidad: fechas })
            .populate('idCliente')
            .exec((err, bolsas) => {
                if (err) {
                    return res.json({
                        ok: false,
                        err
                    });
                }
                if (bolsas.length === 0) {
                    return res.json({
                        ok: false,
                        message: 'No existen bolsas vigentes'
                    });
                }
                return res.json({
                    ok: true,
                    bolsas
                });
            });
    }
});



app.get('/sis-fidelizacion/bolsa-puntos', function(req, res) {
    BolsaPuntos.find({})
        .populate('idCliente')
        .exec((err, bolsas) => {
            if (err) {
                return res.json({
                    ok: false,
                    err
                });
            }
            res.json({
                ok: true,
                bolsas
            });
        });
});

app.get('/sis-fidelizacion/bolsa-puntos/cliente/:id', function(req, res) {
    let idCliente = req.params.id;
    console.log(mongoose.Types.ObjectId.isValid(req.params.id));
    if (!mongoose.Types.ObjectId.isValid(req.params.id)) {
        return res.json({
            ok: false,
            message: 'id invalido'
        });
    }
    BolsaPuntos.find({
            idCliente: req.params.id
        })
        .populate('idCliente')
        .exec((err, bolsas) => {
            if (err) {
                return res.json({
                    ok: false,
                    err
                });
            }
            console.log(bolsas.length);
            if (bolsas.length === 0) {
                return res.json({
                    ok: false,
                    message: 'No existen bolsas para el cliente especificado'
                });
            }
            res.status(200).json({
                ok: true,
                bolsas
            });
        });
});

app.get('/sis-fidelizacion/bolsa-puntos/vencidos', function(req, res) {
    let fecha = new Date();
    console.log(fecha);


    BolsaPuntos.find({ fechaCaducidad: { $gte: fecha } })
        .populate('idCliente')
        .exec((err, bolsas) => {
            if (err) {
                return res.json({
                    ok: false,
                    err
                });
            }
            if (bolsas.length === 0) {
                return res.json({
                    ok: false,
                    message: 'No existen bolsas vencidas'
                });
            }
            res.json({
                ok: true,
                bolsas
            });
        });
});


app.get('/sis-fidelizacion/bolsa-puntos/vigentes', function(req, res) {
    let fecha = new Date();
    console.log(fecha);

    BolsaPuntos.find({ fechaCaducidad: { $lte: fecha } })
        .populate('idCliente')
        .exec((err, bolsas) => {
            if (err) {
                return res.json({
                    ok: false,
                    err
                });
            }
            if (bolsas.length === 0) {
                return res.json({
                    ok: false,
                    message: 'No existen bolsas vigentes'
                });
            }
            res.json({
                ok: true,
                bolsas
            });
        });
});

app.get('/sis-fidelizacion/bolsa-puntos/vencimiento/:dias', function(req, res) {
    console.log(req.params);
    let dias = Number(req.params.dias)
    console.log(dias);
    if (isNaN(dias)) {
        return res.json({
            ok: false,
            message: 'El numero de dias no es valido'
        });
    }
    let fecha = new Date();
    console.log(fecha);
    addSubstractDate.add(fecha, dias, "days")
    console.log(fecha);

    BolsaPuntos.find({ fechaCaducidad: fecha })
        .populate('idCliente')
        .exec((err, bolsas) => {
            if (err) {
                return res.json({
                    ok: false,
                    err
                });
            }
            if (bolsas.length === 0) {
                return res.json({
                    ok: false,
                    message: 'No existen bolsas con esa fecha de vencimiento'
                });
            }
            res.json({
                ok: true,
                bolsas
            });
        });
});

app.post('/sis-fidelizacion/bolsa-puntos/cargar', function(req, res) {
    body = req.body;

    /***
     * monto: monto de la operacion
     * idCliente: identificador del cliente
     */
    let monto = body.monto;
    let idCliente = body.idCliente;
    let fechaAsignacion = new Date();
    VencimientoPuntos.find({
        fechaInicio: { $lte: fechaAsignacion },
        fechaFin: { $gte: fechaAsignacion }
    }).exec((err, listVencimiento) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                err
            });
        }
        if (listVencimiento.length === 0) {
            return res.json({
                ok: false,
                message: 'No se puede obtener la duracion para la fecha de hoy'
            });
        }
        console.log(listVencimiento[0]);
        let vencimiento = listVencimiento[0];
        console.log(vencimiento.diasDuracion);
        ReglasAsignacion.find({
            limiteInferior: { $lte: monto },
            limiteSuperior: { $gte: monto }
        }).exec((err, listaReglas) => {
            console.log('entra a buscar las reglas de asignacion');
            if (err) {
                return res.json({
                    ok: false,
                    err
                });
            }
            console.log(listaReglas);
            if (listaReglas.length === 0) {
                return res.json({
                    ok: false,
                    message: 'No se pudieron encontrar un limite al cual equivalga el monto'
                });
            }
            console.log(listaReglas);
            let montoEquivalencia = listaReglas[0].montoEquivalencia;
            //Se obtiene el la cantidad de puntos
            console.log(montoEquivalencia);
            console.log(Math.round(monto / montoEquivalencia));

            let cantPuntos = Math.round(monto / montoEquivalencia);
            console.log(cantPuntos);
            let fechaVencimiento = new Date();
            console.log(fechaVencimiento);
            addSubstractDate.add(fechaVencimiento, vencimiento.diasDuracion, "days")
            console.log(fechaVencimiento);
            let bolsaPuntos = new BolsaPuntos({
                idCliente: idCliente,
                fechaAsignacion: fechaAsignacion,
                fechaCaducidad: fechaVencimiento,
                puntajeAsignado: cantPuntos,
                puntajeUtilizado: 0,
                saldoPuntos: cantPuntos,
                montoOperacion: monto
            });
            bolsaPuntos.save((err, bolsaPuntosDB) => {
                if (err) {
                    return res.json({
                        ok: false,
                        err
                    });
                }
                console.log('Bolsa de puntos creada');
                res.json({
                    ok: true,
                    bolsa: bolsaPuntosDB
                });
            });
            console.log(bolsaPuntos);
        });

    });
});
module.exports = app;