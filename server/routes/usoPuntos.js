const express = require('express');

const app = express();

const UsoPuntos = require('../models/conceptoPuntos');

app.get('/sis-fidelizacion/puntos', function(req, res) {
    UsoPuntos.find({})
        .exec((err, usoDePuntos) => {
            if (err) {
                return res.status(400).json({
                    ok: false,
                    err
                })
            }
            return res.json({
                ok: true,
                usoPuntos: usoDePuntos
            })
        });

});


app.get('/sis-fidelizacion/puntos/:id', function(req, res) {
    console.log(req.params);
    let id = req.params.id;
    UsoPuntos.findById(id, (err, usoPuntosDB) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                err
            });
        }
        res.json({
            ok: true,
            usoDePuntos: usoPuntosDB
        });
    });
});




app.post('/sis-fidelizacion/puntos', function(req, res) {
    let body = req.body;
    console.log(body);

    let usoPuntos = new UsoPuntos({
        descripcion: body.descripcion,
        puntosRequeridos: body.puntosRequeridos
    });

    console.log(usoPuntos);

    usoPuntos.save((err, usoPuntosDB) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                err
            })
        }
        res.json({
            ok: true,
            usoPuntos: usoPuntosDB
        })
    })

});

app.put('/sis-fidelizacion/puntos/:id', function(req, res) {
    let id = req.params.id;
    let body = req.body;

    UsoPuntos.findByIdAndUpdate(id, body, { new: true, runValidators: true }, (err, usoPuntosDB) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                err
            });
        }
        res.json({
            ok: true,
            usoPuntosDB
        })
    });
});
app.delete('/sis-fidelizacion/puntos/:id', function(req, res) {
    let id = req.params.id;
    UsoPuntos.findByIdAndDelete(id, (err, usoPuntosDB) => {
        if (err) {
            return res.json({
                ok: false,
                err
            });
        }
        res.json({
            ok: true,
            usoPunto: usoPuntosDB
        });
    })
});

module.exports = app;